//
//  ViewController.swift
//  CICE_PRACTICA_18_iOS_COREDATA_PUSH
//
//  Created by Diego Angel Fernandez Garcia on 23/04/2019.
//  Copyright © 2019 Diego Angel Fernandez Garcia. All rights reserved.
//

import UIKit
import UserNotifications
import CoreLocation
import MapKit

class ViewController: UIViewController {

    var locationManager = CLLocationManager()
    let searchRequest = MKLocalSearch.Request()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        //geofenceNotification()
        
        
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [UNAuthorizationOptions.alert, .sound]) { (granted, error) in
            if granted{
                print ("todo ok")
            } else {
                print("todo mal")
            }
        }
    }

    func disparadorTemporal(notificacion: UNMutableNotificationContent, center: UNUserNotificationCenter) {
        //Creamos un disparador en base a un intervalo de tiempo
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        //Creamos una petición y la envamos al NotificationCenter
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: notificacion, trigger: trigger)
        center.add(request, withCompletionHandler: nil)
    }
    
    //Por Geolocalización
    func geofenceNotification(buscar: String) {
        //Definimos lo que vamos a buscar en una zona concreta
        searchRequest.naturalLanguageQuery = buscar
        
        //Obtenemos nuestras coordenadas
        
        if CLLocationManager.locationServicesEnabled() == true {
            
        } else {
        print("Error: Active la localizacion por GPS")
        alerta(titulo: "Error Señal GPS", contenido: "Active la localizacion por GPS")
        }
        
        guard let miPosicion = locationManager.location?.coordinate else {
            print("Error: no se encuentran coordenadas GPS")
            alerta(titulo: "Error Señal GPS", contenido: "no se encuentran coordenadas GPS")
            return
        }
    
    
       // let miPosicion = CLLocationCoordinate2DMake(37.331705, -122.030237)
        //Definimos una zona a nuestro arrededor
        searchRequest.region = MKCoordinateRegion(center: miPosicion, latitudinalMeters: 1000, longitudinalMeters: 1000)
        let search = MKLocalSearch(request: searchRequest)
        
        //Realizamos la consulta de los restaurantes cercanos
        search.start { response, error in
            guard let response = response else {
                print("Error: \(error?.localizedDescription ?? "Unknown error").")
                return
            }
            //Como maximo se monitorizamos 20 ubicaciones
            var cont = 0
            var contenidoAlerta = "\n"
            for item in response.mapItems {
                if cont <= 20 {
                print ("Se activa la region:", item.name ?? "No name.", item.placemark.coordinate)
                contenidoAlerta += ("\(buscar): \(item.name ?? "No name.")\n")
                contenidoAlerta += ("-----------\n")
                
                //Propiedades
                let geofenceRegion = CLCircularRegion(center: item.placemark.coordinate, radius: 100, identifier: item.name ?? "no name")
                
                //Definimos cuándo queremos ser notificados
                geofenceRegion.notifyOnEntry = true
                geofenceRegion.notifyOnExit = true
                
                    
                //Elimino las regiones monitorizadas anteriormente si las hay
                    if self.locationManager.monitoredRegions.count > 0 {
                        let regionesMonitorizadas = self.locationManager.monitoredRegions
                        for region in regionesMonitorizadas{
                        self.locationManager.stopMonitoring(for: region)
                        }
                    }
                //Monitorizamos la región (Máx 20).
                self.locationManager.startMonitoring(for: geofenceRegion)
                
                }else{
                    return
                }
                cont += 1
            }
            self.alerta(titulo: "Se monitorizaran las siguientes ubicaciones: ", contenido: contenidoAlerta)
        }
        
    }
    //Funcion generica para mostra alertas
    func alerta( titulo: String, contenido: String){
        let alertController = UIAlertController(title: titulo, message:
            contenido, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default))
        
        self.present(alertController, animated: true, completion: nil)    }
    
    
    @IBAction func monitorizarRestaurantes(_ sender: Any) {
        geofenceNotification(buscar: "Restaurante")
        
    }
    @IBAction func monitorizarTienadas(_ sender: Any) {
        geofenceNotification(buscar: "Tienda")
        
    }
    @IBAction func monitorizarCafeterias(_ sender: Any) {
        geofenceNotification(buscar: "Cafeteria")
       
    }
    @IBAction func monitorizarFarmacias(_ sender: Any) {
        geofenceNotification(buscar: "Farmacia")
        
    }
}



extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        print("Hemos entrado en la región!")
        //Creamos la notificación
        let center = UNUserNotificationCenter.current()
        let notificacion = UNMutableNotificationContent()
        //Definimos el contenido de la notificación
        notificacion.title = "El Restaurante \(region.identifier) esta muy cerca"
        notificacion.body = "Entra y aprobecha las ofertas"
        notificacion.categoryIdentifier = region.identifier
        notificacion.sound = UNNotificationSound.default
        
        disparadorTemporal(notificacion: notificacion, center: center)
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        print("Vuelve pronto")
    }
    
    /*
     //funcion que ejecuta la busqueda de restaurantes cercanos para monitorizar al cambiar nuesta posicion GPS
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        geofenceNotification("Restaurante")
    }
     */
}
